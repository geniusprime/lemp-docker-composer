# Docker Compose LEMP Stack

This repository contains a little `docker-compose` configuration to start a `LEMP (Linux, Nginx, MariaDB, PHP)` stack.

## Details

The following versions are used.

* PHP 7.3 (FPM)
* Nginx 1.17.8
* MariaDB 10.5.1

## Configuration

The Nginx configuration can be found in `config/nginx/`.

You can also set the following environment variables, create `.env` file see, `.env.sample`:

* APP - The name used when creating a container
* MYSQL_ROOT_PASSWORD - The MySQL root password used when creating the container.


## Usage

To use it, simply follow the following steps:

##### Start the server.

Start the server using the following command inside the directory you just cloned: `docker-compose up`.

## Entering the containers

You can use the following command to enter a container:

Where `{CONTAINER_NAME}` is one of:

`docker exec -ti {CONTAINER_NAME} /bin/bash`

* `{APP}-php`
* `{APP}-nginx`
* `{APP}-mariadb`
